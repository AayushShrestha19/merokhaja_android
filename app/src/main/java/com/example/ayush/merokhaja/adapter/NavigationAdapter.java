package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.activity.Dashboard;
import com.example.ayush.merokhaja.model.NavigationItem;

import java.util.List;

public class NavigationAdapter  extends BaseAdapter {

    private Context context;
    private List<NavigationItem>navigationItemList;


    public NavigationAdapter(Context context,List<NavigationItem> navigationItemList){
        this.context=context;
        this.navigationItemList=navigationItemList;
    }
    @Override
    public int getCount() {
        return navigationItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView==null){
            convertView = layoutInflater.inflate(R.layout.navigation_list_view_items,null,false);

        }
        ImageView navImageIV = convertView.findViewById(R.id.navItemImageIV);
        TextView navTitleTV=convertView.findViewById(R.id.navItemTitleTV);


        NavigationItem navigationItem = navigationItemList.get(position);
        navTitleTV.setText(navigationItem.getNavItemTitleTV());
        navImageIV.setImageResource(navigationItem.getNavItemImageIV());
        navImageIV.setColorFilter(R.color.colorPrimary);
        return convertView;
    }
}
