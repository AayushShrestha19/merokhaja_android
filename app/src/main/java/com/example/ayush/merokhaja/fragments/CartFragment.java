package com.example.ayush.merokhaja.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.adapter.CartRecyclerViewAdapter;
import com.example.ayush.merokhaja.database.CartFood;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.databaseHandler.DBCartFoodHandler;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.apiHandlers.FoodApiHandler;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {
    private View view;
    public RecyclerView cartRecyclerView;
    private TextView toobartitle;
    private Button buyBtn;
    private static final int REQUEST_CALL=1;
    private static final String  NUMBER="9843548792";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cart, container, false);
        initiliseViews();
        configureRecyclerView();
        setOnCLickListner();
        return view;
    }



    private void configureRecyclerView() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        cartRecyclerView.setLayoutManager(linearLayoutManager);
        List<CartFood> cartFoodList = new DBCartFoodHandler().getAllFood();
        CartRecyclerViewAdapter adapter = new CartRecyclerViewAdapter(getContext(), cartFoodList);
        cartRecyclerView.setAdapter(adapter);
    }

    private void initiliseViews() {

        cartRecyclerView = view.findViewById(R.id.cartrecyclerviewRV);
        toobartitle = view.findViewById(R.id.toolbartital);
        buyBtn=view.findViewById(R.id.buyBtn);
    }
    private void setOnCLickListner() {
        buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCall();
            }
        });
    }

    private void makePhoneCall() {
        Toast.makeText(getContext(), "call vayana", Toast.LENGTH_SHORT).show();
        String number = NUMBER.toString();
        if (ContextCompat.checkSelfPermission(getContext(),Manifest.permission.CALL_PHONE) !=PackageManager.PERMISSION_GRANTED){
            Toast.makeText(getContext(), "call ", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CALL_PHONE},REQUEST_CALL);
        } else
        {
            Toast.makeText(getContext(), "call vayana", Toast.LENGTH_SHORT).show();
            String dial = "tel."+number;
            startActivity(new Intent(Intent.ACTION_CALL,Uri.parse(number)));
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_CALL){
             if (grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                 makePhoneCall();
             }
             else {
                 Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
             }
        }
    }
}
