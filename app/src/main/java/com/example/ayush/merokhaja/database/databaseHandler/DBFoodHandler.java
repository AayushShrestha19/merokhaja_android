package com.example.ayush.merokhaja.database.databaseHandler;

import com.example.ayush.merokhaja.database.Food;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

public class DBFoodHandler {

    public List<Food> getAllFood() {
        List<Food> dbFoodList = Food.listAll(Food.class);
        if (dbFoodList == null) {
            dbFoodList = new ArrayList<>();
        }
        return dbFoodList;
    }

    public List<Food> getchowmien() {
        List<Food> foodList = Food.findWithQuery(Food.class, "SELECT *" +
                "FROM Food" + "WHERE foodname LIKE" +
                "Chowmien");
        return foodList;
    }

    public void deleteFood() {
        Food.deleteAll(Food.class);
    }

    public List<Food> checkData(String search) {
        List<Food> foodList = Food.findWithQuery(Food.class, "SELECT *" +
                "  FROM Food" +
                " WHERE Name LIKE " + "?", search);

        return foodList;
    }
}
