package com.example.ayush.merokhaja.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ayush.merokhaja.R;

public class SignUp extends AppCompatActivity {

    private EditText usernameET, passwordET;
    private Button signInBtn;
    private TextView anAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initializeViews();
        initializeListeners();

        usernameET.setText("aaush");
        passwordET.setText("aaush");
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUser(usernameET.getText().toString(), passwordET.getText().toString());
            }
        });


    }

    private void initializeListeners() {
        anAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUp.this,Login.class));
            }
        });
    }

    private void validateUser(String username, String password) {
        if (!username.equalsIgnoreCase("") && !password.equalsIgnoreCase("")) {
            if (username.equalsIgnoreCase("aaush") && password.equalsIgnoreCase("aaush")) {
                Intent intent = new Intent(SignUp.this, SearchActivity.class);
                startActivity(intent);

            } else {
                Toast.makeText(this, "wrong username or password", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "empty uername or password", Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeViews() {
        usernameET = findViewById(R.id.usernameET);
        passwordET = findViewById(R.id.passwordET);
        signInBtn = findViewById(R.id.signUpBtn);
        anAccount = findViewById(R.id.anaccount);
    }






    /*private void initializeListeners() {
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user  = new User();
                final String usernameEt=usernameET.getText().toString();
                final String passwordEt=passwordET.getText().toString();
                user.setUsername(usernameEt);
                user.setPassword(passwordEt);

                LoginApiHandler loginApiHandler = new LoginApiHandler();
                loginApiHandler.login(SignUp.this,user, new ApiListner() {
                    @Override
                    public void success(Object responce) {
                        JSONObject jsonObject=new JSONObject();
                        String username,password;
                        try {
                            username=jsonObject.getString("username");
                            password=jsonObject.getString("password");

                            Toast.makeText(SignUp.this, "username is :"+ username, Toast.LENGTH_SHORT).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void failure(Object responce) {
                        Toast.makeText(SignUp.this, "Error Occorrd" + responce.toString(), Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });
    }*/


}
