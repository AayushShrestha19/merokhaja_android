package com.example.ayush.merokhaja.database;

import com.orm.SugarRecord;

import java.io.Serializable;

public class FavoriteFood extends SugarRecord implements Serializable {
    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    public int getFoodcost() {
        return foodcost;
    }

    public void setFoodcost(int foodcost) {
        this.foodcost = foodcost;
    }

    public String getFoodpic() {
        return foodpic;
    }

    public void setFoodpic(String foodpic) {
        this.foodpic = foodpic;
    }

    private String foodname;
    private int foodcost;
    private String foodpic;
}
