

package com.example.ayush.merokhaja.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.adapter.MenuRecylerViewAdapter;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.apiHandlers.FoodApiHandler;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MenuFragment extends Fragment {

    private View view;
    private RecyclerView menuRecyclerView;
    private ProgressDialog progressDialog;
    SweetAlertDialog sweetAlertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_menu, container, false);
        initializeView();
        configureProgressDialog();
        configureSweetAlerLoading();
//        configureSweetAlertDialog();
        getFoodFromServer();
        initializeClickListeners();
        return view;
    }

    private void initializeClickListeners() {


    }


    private void configureSweetAlerLoading() {
        sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading Foods...");
    }


    private void configureProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
    }

    private void showProgressDialog() {
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void getFoodFromServer() {
//        showProgressDialog();
        sweetAlertDialog.show();
        new FoodApiHandler().getFoodList(getContext(), new ApiListner() {
            @Override
            public void success(Object responce) {
//                hideProgressDialog();
                sweetAlertDialog.dismiss();
                configureRecyclerView();
            }

            @Override
            public void failure(Object responce) {
//                hideProgressDialog();
                sweetAlertDialog.dismiss();
            }
        });
    }

    private void initializeView() {
        menuRecyclerView = view.findViewById(R.id.menuRecyclerView);

    }

    private void configureRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        final List<Food> foodList = new DBFoodHandler().getAllFood();
        MenuRecylerViewAdapter adapter = new MenuRecylerViewAdapter(getContext(), foodList);
        menuRecyclerView.setLayoutManager(manager);
        menuRecyclerView.setAdapter(adapter);

//        adapter.setRecyclerItemClickListener(new CustomRecyclerClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                Intent foodDetail = new Intent(getContext(), FoodDescription.class);
//                Food food = foodList.get(position)
//                foodDetail.putExtra(Constants.Bundle_Key.FOOD_DETAIL, food);
//                startActivity(foodDetail);
//            }
//        });
    }


}

