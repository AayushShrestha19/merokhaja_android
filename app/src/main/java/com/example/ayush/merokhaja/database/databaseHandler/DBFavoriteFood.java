package com.example.ayush.merokhaja.database.databaseHandler;

import com.example.ayush.merokhaja.database.FavoriteFood;

import java.util.ArrayList;
import java.util.List;

public class DBFavoriteFood {
    public List<FavoriteFood> getAllFood() {
        List<FavoriteFood> dbFavoriteList = FavoriteFood.listAll(FavoriteFood.class);
        if (dbFavoriteList == null) {
            dbFavoriteList = new ArrayList<>();
        }
        return dbFavoriteList;
    }
}

