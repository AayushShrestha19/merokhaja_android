package com.example.ayush.merokhaja.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.adapter.ExpandedFoodMenuAdapter;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.utils.Constants;

import java.util.List;

public class FoodList extends AppCompatActivity {
    private RecyclerView expandedfoodmenuitem;
    List<Food> foodList;
    private TextView titlename;
    private ImageView backbutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_foodlist);

        initializedViews();
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent().getExtras() != null) {
            foodList = (List<Food>) getIntent().getExtras().get(Constants.Bundle_Key.FOOD_DETAIL);
//            Log.e("count","size" +foodList.size());

            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(FoodList.this,2);
            ExpandedFoodMenuAdapter adapter=new ExpandedFoodMenuAdapter(FoodList.this, foodList);
            expandedfoodmenuitem.setLayoutManager(layoutManager);
            expandedfoodmenuitem.setAdapter(adapter);
        }

    }

    private void initializedViews() {
        expandedfoodmenuitem=findViewById(R.id.selectedFoodRecyclerView);
        titlename=findViewById(R.id.titlenameTV);
        backbutton=findViewById(R.id.backbuttonIV);
    }
}
