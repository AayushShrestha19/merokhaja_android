package com.example.ayush.merokhaja.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.activity.Account;
import com.example.ayush.merokhaja.activity.Login;
import com.example.ayush.merokhaja.database.User;
import com.example.ayush.merokhaja.database.databaseHandler.DBUserHandler;
import com.example.ayush.merokhaja.support.AppController;
import com.example.ayush.merokhaja.utils.Constants;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private TextView profilenameTV;
    private ImageView profilepictureIV;
    private TextView profileemailTV;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private RelativeLayout editprofile, logout;
    SweetAlertDialog sweetAlertDialog;
    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        initializeViews();
        setuserdata();
        configureSweetAlerLoading();
        setOnclickListner();

        return view;
    }


        private void configureSweetAlerLoading() {
            sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE)
                    .setTitleText("Logging Out...");
        }


    private void setOnclickListner() {
        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Account.class));
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             sweetAlertDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sweetAlertDialog.dismiss();
                        AppController.getInstance().deleteAllDBRecords();
                        prefs= PreferenceManager.getDefaultSharedPreferences(getContext());
                        editor= prefs.edit();
                        editor.putBoolean(Constants.SharedPreferenceConstants.HAS_USER, false);
                        editor.apply();
                        startActivity(new Intent(getContext(), Login.class));
                        getActivity().finish();
                    }
                }, 2000);
            }
        });
    }

    private void setuserdata() {
        User user = new DBUserHandler().getAllUsers().get(0);
        profilenameTV.setText(user.getName());
        profileemailTV.setText(user.getEmail());
        Glide.with(getContext()).load(user.getImage()).into(profilepictureIV);

    }


    private void initializeViews() {
        profileemailTV = view.findViewById(R.id.profileemailTV);
        profilenameTV = view.findViewById(R.id.profilenameTV);
        profilepictureIV = view.findViewById(R.id.profilepictureIV);
        editprofile = view.findViewById(R.id.editprofile);
        logout = view.findViewById(R.id.logout);

    }

}
