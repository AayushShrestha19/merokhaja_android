package com.example.ayush.merokhaja.interfaces;

import android.view.View;

public interface CustomRecyclerClickListener {
    void onClick(View view,int position);
}
