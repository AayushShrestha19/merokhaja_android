package com.example.ayush.merokhaja.support.apiHandlers;

import android.content.Context;
import android.util.Log;
import android.view.Menu;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.AppController;
import com.example.ayush.merokhaja.utils.Constants;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FoodApiHandler {
    private String TAG = "FoodApiHandler";

    public void getFoodList(Context context, final ApiListner apiListner) {
        String url = Constants.MeroKhaja_Url.BASE_URL + Constants.MeroKhaja_Url.FOOD_URL;
        Log.e(TAG, "food url: " + url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                 Log.e(TAG, "food response : " + response);
                new DBFoodHandler().deleteFood();
                try {
                    List<Food> foodList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray menuArray = jsonObject.getJSONArray("menu");
                    for (int i = 0; i < menuArray.length(); i++) {

                        JSONObject foodJsonObj = menuArray.getJSONObject(i);
                        Food food = new Food();
                        food.setFoodname(foodJsonObj.getString("Name"));
                        food.setFoodcost(Integer.parseInt(foodJsonObj.getString("Price")));
                        food.setFoodpic(foodJsonObj.getString("Picture"));
                        food.setDescription(foodJsonObj.getString("Description"));
                        foodList.add(food);
                    }
                    SugarRecord.saveInTx(foodList);
                    apiListner.success("success");
                } catch (JSONException e) {
                    e.printStackTrace();
                    apiListner.failure("failed");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                apiListner.failure("failed : " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
