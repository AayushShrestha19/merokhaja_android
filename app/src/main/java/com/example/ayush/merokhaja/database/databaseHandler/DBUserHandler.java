package com.example.ayush.merokhaja.database.databaseHandler;

import com.example.ayush.merokhaja.database.User;

import java.util.ArrayList;
import java.util.List;

public class DBUserHandler {

    public boolean checkUser(String email, String password) {
        List<User> userList = User.find(User.class, "email = ? AND password = ?", email, password);
        return userList.size() > 0;
    }

    public User getUser(String email, String password) {
        List<User> userList = User.find(User.class, "email = ? AND password = ?", email,password);
        if (userList == null) {
            userList = new ArrayList<>();
        }
        return userList.get(0);
    }

    public List<User> getAllUsers() {
        List<User> dbUserList = User.listAll(User.class);
        if (dbUserList == null) {
            dbUserList = new ArrayList<>();
        }
        return dbUserList;
    }

    public static void deleteUsers() {
        User.deleteAll(User.class);
    }
}
