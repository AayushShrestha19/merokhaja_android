package com.example.ayush.merokhaja.model;

public class NavigationItem {
    private String navItemTitleTV;
    private Integer navItemImageIV;

    public String getNavItemTitleTV() {
        return navItemTitleTV;
    }

    public void setNavItemTitleTV(String navItemTitleTV) {
        this.navItemTitleTV = navItemTitleTV;
    }

    public Integer getNavItemImageIV() {
        return navItemImageIV;
    }

    public void setNavItemImageIV(Integer navItemImageIV) {
        this.navItemImageIV = navItemImageIV;
    }






}
