package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.FoodType;

import java.util.List;

public class MenuListViewAdapter extends BaseAdapter {
    private Context context;
    private List<FoodType> foodList;
    private RelativeLayout menuParentLayoutLV;

    public MenuListViewAdapter(Context context,List<FoodType>foodList){
        this.context=context;
        this.foodList=foodList;

    }
    @Override
    public int getCount() {
        return foodList.size();
    }

    @Override
    public Object getItem(int position) {
        return foodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.food_items_list_view,null);

        TextView menufoodTV = convertView.findViewById(R.id.menufoodTV);
        ImageView menupicIV=convertView.findViewById(R.id.menupicIV);

        menuParentLayoutLV=convertView.findViewById(R.id.menuParentLayoutLV);
        FoodType food = foodList.get(position);
        menufoodTV.setText(food.getName());
        Glide.with(context).load(food.getPicture()).into(menupicIV);



        return convertView;
    }


}
