package com.example.ayush.merokhaja.database;

import com.orm.SugarRecord;

import java.io.Serializable;

public class FoodType extends SugarRecord implements Serializable {
    private String Name;
    private String Picture;
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }



}
