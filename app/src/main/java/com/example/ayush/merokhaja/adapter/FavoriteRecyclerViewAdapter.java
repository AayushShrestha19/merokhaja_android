package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.database.FavoriteFood;

import java.util.List;

public class FavoriteRecyclerViewAdapter extends RecyclerView.Adapter<FavoriteRecyclerViewAdapter.FavoriteRecyclerViewHolder> {
    private Context context;
    private List<FavoriteFood> favoriteFoodList;

    public FavoriteRecyclerViewAdapter(Context context, List<FavoriteFood> favoriteFoodList){
        this.context= context;
        this.favoriteFoodList=favoriteFoodList;

    }
    @NonNull
    @Override
    public FavoriteRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.favorite_item_view,viewGroup,false);
        FavoriteRecyclerViewHolder favoriteRecyclerViewHolder = new FavoriteRecyclerViewHolder(view);
        return favoriteRecyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteRecyclerViewHolder favoriteRecyclerViewHolder, int position) {
        FavoriteFood favoriteFood = favoriteFoodList.get(position);
        favoriteRecyclerViewHolder.foodnameTV.setText(favoriteFood.getFoodname());
        favoriteRecyclerViewHolder.foodPriceTV.setText("Rs." + favoriteFood.getFoodcost());
        Glide.with(context).load(favoriteFood.getFoodpic()).into(favoriteRecyclerViewHolder.foodPicIV);
    }

    @Override
    public int getItemCount() {
        return favoriteFoodList.size();
    }

    public class FavoriteRecyclerViewHolder extends RecyclerView.ViewHolder {

        private TextView foodnameTV;
        private ImageView foodPicIV;
        private TextView foodPriceTV;
        public FavoriteRecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            foodnameTV = itemView.findViewById(R.id.favoritefoodnameTV);
            foodPriceTV = itemView.findViewById(R.id.favoritefoodpriceTV);
            foodPicIV = itemView.findViewById(R.id.favoritefoodpiIV);
        }
    }
}
