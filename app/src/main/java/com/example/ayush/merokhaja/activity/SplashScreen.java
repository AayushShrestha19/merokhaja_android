package com.example.ayush.merokhaja.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.utils.Constants;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);
                if (sharedPreferences.contains(Constants.SharedPreferenceConstants.HAS_USER)) {
                    //user already logged in
                    if (sharedPreferences.getBoolean(Constants.SharedPreferenceConstants.HAS_USER, false)) {
                        startActivity(new Intent(SplashScreen.this, Dashboard.class));
                        finish();
                    } else {
                        // user not logged in
                        startActivity(new Intent(SplashScreen.this, LandingPage.class));
                        finish();
                    }

                } else {
                    // user not logged in
                    startActivity(new Intent(SplashScreen.this, LandingPage.class));
                    finish();
                }
            }
        }, 3000);
    }
}
