package com.example.ayush.merokhaja.database;

import com.orm.SugarRecord;

import java.io.Serializable;

public class Food extends SugarRecord implements Serializable {
    private String foodname;
    private int foodcost;
    private String foodpic;
    private String description;

    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    public int getFoodcost() {
        return foodcost;
    }

    public void setFoodcost(int foodcost) {
        this.foodcost = foodcost;
    }

    public String getFoodpic() {
        return foodpic;
    }

    public void setFoodpic(String foodpic) {
        this.foodpic = foodpic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
