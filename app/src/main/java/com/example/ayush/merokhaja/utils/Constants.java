package com.example.ayush.merokhaja.utils;

public class Constants {
    public static class MeroKhaja_Url {
//       eg : https://script.google.com/macros/s/AKfycbxOLElujQcy1-ZUer1KgEvK16gkTLUqYftApjNCM_IRTL3HSuDk/exec?id=1TadszqmLKjUX8Ri4YGsLjJ9DMLLnT0MMcl1VAmkN8iY&sheet=Sheet1

//        public String BASE_URL= "https://script.google.com/macros/s/AKfycbxOLElujQcy1-ZUer1KgEvK16gkTLUqYftApjNCM_IRTL3HSuDk/exec?id=18pMHK3HjI0MIFO7Rae-Z5TAgfYWnZ64K9sddeg5O-4w" ;
//
//        public String GOOGLE_DRIVE_KEY ="18pMHK3HjI0MIFO7Rae-Z5TAgfYWnZ64K9sddeg5O-4w";
//        public String URL_LOGIN= "login";

        public static String BASE_URL = "https://script.google.com/macros/s/AKfycbxOLElujQcy1-ZUer1KgEvK16gkTLUqYftApjNCM_IRTL3HSuDk/exec?id=18pMHK3HjI0MIFO7Rae-Z5TAgfYWnZ64K9sddeg5O-4w&sheet=";

        public static String LOGIN_URL = "login";
        public static String FOOD_URL = "menu";
        public static String FOODTYPE_URL="FoodType";


    }

    public static class Bundle_Key{
        public static String FOOD_DETAIL="FOOD_DETAIL";
    }

    public class PermissionConstants {

    }

    public class Location {

    }

    public static class SharedPreferenceConstants {
        public static  String HAS_USER = "HAS_USER";
        public static  String NOTIFICATION_TOKEN ="NOTIFICATION_TOKEN";

    }
}
