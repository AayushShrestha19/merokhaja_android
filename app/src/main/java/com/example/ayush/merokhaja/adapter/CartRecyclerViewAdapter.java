package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.database.CartFood;
import com.example.ayush.merokhaja.database.Food;

import java.util.List;

public class CartRecyclerViewAdapter extends RecyclerView.Adapter<CartRecyclerViewAdapter.CartFoodViewHolder> {
    private Context context;
    private List<CartFood> cartFoodList;
    private static int counter = 1;

    public CartRecyclerViewAdapter(Context context, List<CartFood> cartFoodList) {
        this.context = context;
        this.cartFoodList = cartFoodList;
    }

    @NonNull
    @Override
    public CartFoodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_view_item, viewGroup, false);
        CartFoodViewHolder cartFoodViewHolder = new CartFoodViewHolder(view);
        return cartFoodViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CartFoodViewHolder cartFoodViewHolder, int position) {
        final CartFood food = cartFoodList.get(position);
        cartFoodViewHolder.foodnameTV.setText(food.getFoodname());
        cartFoodViewHolder.foodPriceTV.setText("Rs." + food.getFoodcost());
        Glide.with(context).load(food.getFoodpic()).into(cartFoodViewHolder.foodPicIV);
//        cartFoodViewHolder.toolbar.setText("My Cart");

        cartFoodViewHolder.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (counter < 15) {
                    counter++;
                    cartFoodViewHolder.quantityTV.setText(String.valueOf(counter));
                    cartFoodViewHolder.foodPriceTV.setText("Rs." + String.valueOf(food.getFoodcost() * counter));
                } else {
                    Toast.makeText(context, "you have reached max order for this item.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cartFoodViewHolder.minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter > 1) {
                    counter--;
                    cartFoodViewHolder.quantityTV.setText(String.valueOf(counter));
                    cartFoodViewHolder.foodPriceTV.setText("Rs." + String.valueOf(food.getFoodcost() * counter));
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return cartFoodList.size();
    }

    public class CartFoodViewHolder extends RecyclerView.ViewHolder {
        private TextView foodnameTV;
        private ImageView foodPicIV;
        private TextView foodPriceTV;
        private TextView toolbar;
        private ImageView addBtn, minusBtn;
        private TextView quantityTV;

        public CartFoodViewHolder(@NonNull View itemView) {
            super(itemView);
            foodnameTV = itemView.findViewById(R.id.cartfoodnameTV);
            foodPriceTV = itemView.findViewById(R.id.cartfoodpriceTV);
            foodPicIV = itemView.findViewById(R.id.cartfoodpiIV);
            toolbar = itemView.findViewById(R.id.toolbartital);
            addBtn = itemView.findViewById(R.id.addBtn);
            minusBtn = itemView.findViewById(R.id.minusBtn);
            quantityTV = itemView.findViewById(R.id.quantityTV);
        }
    }
}
