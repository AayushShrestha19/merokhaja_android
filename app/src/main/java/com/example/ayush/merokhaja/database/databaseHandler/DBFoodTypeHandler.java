package com.example.ayush.merokhaja.database.databaseHandler;

import com.android.volley.Response;
import com.example.ayush.merokhaja.database.FoodType;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DBFoodTypeHandler {
    public List<FoodType> getAllFood() {
        List<FoodType> dbfoodlist = FoodType.listAll(FoodType.class);
        if (dbfoodlist == null) {
            dbfoodlist = new ArrayList<>();
        }
        return dbfoodlist;
    }

    public void deleteFoodType(){
        FoodType.deleteAll(FoodType.class);

    }
}
