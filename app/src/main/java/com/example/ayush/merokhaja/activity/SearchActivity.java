package com.example.ayush.merokhaja.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.adapter.MenuRecylerViewAdapter;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodHandler;
import com.example.ayush.merokhaja.interfaces.CustomRecyclerClickListener;
import com.example.ayush.merokhaja.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private EditText searchET;
    private ImageView cancleIV;
    private RecyclerView searchresultRV;
    private RecyclerView menuRecyclerView;
    MenuRecylerViewAdapter menuRecyclerAdapter;
    List<Food> foodList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initializeViews();
        initializeListerners();
        configureRecyclerView();
    }

    private void initializeListerners() {
        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());

            }
        });
        cancleIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchET.getText().clear();
            }
        });

//        searchIC.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                filter(searchET.getText().toString());
//            }
//        });
    }

    private void initializeViews() {
        searchET = findViewById(R.id.searchET);
        cancleIV = findViewById(R.id.cancleIV);
        menuRecyclerView = findViewById(R.id.menuRecyclerView);

    }

    private void configureRecyclerView() {
        LinearLayoutManager manager = new LinearLayoutManager(SearchActivity.this);
        foodList = new DBFoodHandler().getAllFood();
        menuRecyclerAdapter = new MenuRecylerViewAdapter(SearchActivity.this, foodList);
        menuRecyclerView.setLayoutManager(manager);
        menuRecyclerView.setAdapter(menuRecyclerAdapter);

        /*menuRecyclerAdapter.setRecyclerItemClickListener(new CustomRecyclerClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent foodDetail = new Intent(SearchActivity.this, FoodDescription.class);
                foodDetail.putExtra(Constants.Bundle_Key.FOOD_DETAIL, foodList.get(position));
                startActivity(foodDetail);
            }
        });*/
    }

    void filter(String text) {
        List<Food> oldDataList = new DBFoodHandler().getAllFood();
        List <Food> updatedList = new ArrayList<>();
        for (Food food : oldDataList) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (food.getFoodname().toUpperCase().contains(text.toUpperCase())) {
                updatedList.add(food);
//            } else if (food.getFoodcost() < 100) {
//                temp.add(food);
//            }
            }
            //update recyclerview
            menuRecyclerAdapter.updateList(updatedList);
        }
    }
}
