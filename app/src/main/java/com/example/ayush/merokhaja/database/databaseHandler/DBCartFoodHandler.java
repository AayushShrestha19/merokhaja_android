package com.example.ayush.merokhaja.database.databaseHandler;

import com.example.ayush.merokhaja.database.CartFood;

import java.util.ArrayList;
import java.util.List;

public class DBCartFoodHandler  {
    public List<CartFood> getAllFood() {
        List<CartFood> dbCartList = CartFood.listAll(CartFood.class);
        if (dbCartList == null) {
            dbCartList = new ArrayList<>();
        }
        return dbCartList;
    }
}
