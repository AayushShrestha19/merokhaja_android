package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.database.Food;

import java.util.List;

public class SearchRecyclerViewAdapter extends RecyclerView.Adapter<SearchRecyclerViewAdapter.SearchResultViewHolder>{
    private Context context;
    private List<Food> foodList;

    public SearchRecyclerViewAdapter(Context context, List<Food> foodList){
        this.context=context;
        this.foodList=foodList;
    }

    @NonNull
    @Override
    public SearchRecyclerViewAdapter.SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_result_view,viewGroup,false);
        SearchRecyclerViewAdapter.SearchResultViewHolder searchResultViewHolder = new SearchRecyclerViewAdapter.SearchResultViewHolder(view);
        return searchResultViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchRecyclerViewAdapter.SearchResultViewHolder searchResultViewHolder, int position) {
        Food food = foodList.get(position);
        searchResultViewHolder.foodnameTV.setText(food.getFoodname());
        searchResultViewHolder.foodPriceTV.setText(food.getFoodcost());
        Glide.with(context).load(food.getFoodpic()).into(searchResultViewHolder.foodPicIV);


    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class SearchResultViewHolder extends RecyclerView.ViewHolder {
        private TextView foodnameTV;
        private ImageView foodPicIV;
        private TextView foodPriceTV;
        public SearchResultViewHolder(@NonNull View itemView) {
            super(itemView);
            foodnameTV = itemView.findViewById(R.id.searchfoodnameTV);
            foodPriceTV = itemView.findViewById(R.id.searchfoodpriceTV);
            foodPicIV = itemView.findViewById(R.id.searchfoodpiIV);

        }
    }
}
