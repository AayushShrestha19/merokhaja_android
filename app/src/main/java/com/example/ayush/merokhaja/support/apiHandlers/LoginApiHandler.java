package com.example.ayush.merokhaja.support.apiHandlers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ayush.merokhaja.database.User;
import com.example.ayush.merokhaja.database.databaseHandler.DBUserHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.AppController;
import com.example.ayush.merokhaja.utils.Constants;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginApiHandler {

    String TAG = "LoginApiHandler";

    public void login(Context context,/* User user,*/ final ApiListner apiListner) {

        String url = Constants.MeroKhaja_Url.BASE_URL + Constants.MeroKhaja_Url.LOGIN_URL;
        Log.e(TAG, "login url: " + url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "login response: " + response);
//                List<User> userList = new ArrayList<>();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray loginArray = jsonObject.getJSONArray("login");
                    JSONObject loginObj = loginArray.getJSONObject(0);
                    User user = new User();
                    user.setEmail(loginObj.getString("Email"));
                    user.setPassword(loginObj.getString("Password"));
                    user.setName(loginObj.getString("Name"));
                    user.setImage(loginObj.getString("Profile"));
//                    for (int i = 0; i < loginArray.length(); i++) {
//                        JSONObject loginObj = loginArray.getJSONObject(i);
//                        User user = new User();
//                        user.setEmail(loginObj.getString("Email"));
//                        user.setPassword(loginObj.getString("Password"));
//                        user.setName(loginObj.getString("Name"));
//                        user.setImage(loginObj.getString("Profile"));
//                        userList.add(user);
////                                user.save(); // it saves object in database
//                    }
                    new DBUserHandler().deleteUsers();
                    user.save(); // it saves object in database

//                    SugarRecord.saveInTx(userList); // it saves list of object in bulk in database
                    apiListner.success(response);

                } catch (JSONException ex) {
                    Log.e(TAG, "Error: " + ex.getMessage());
                    apiListner.failure(ex.getMessage());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error occord" + error.getMessage());
                apiListner.failure(error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
