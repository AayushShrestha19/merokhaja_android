package com.example.ayush.merokhaja.activity;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.database.CartFood;
import com.example.ayush.merokhaja.database.FavoriteFood;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.databaseHandler.DBCartFoodHandler;
import com.example.ayush.merokhaja.fragments.CartFragment;
import com.example.ayush.merokhaja.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class FoodDescription extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private TextView foodnameTV, foodpriceTv, fooddescriptionTV, titlenameTV;
    private ImageView foodpictureIV;
    private Button addtocartBtn, addtofavoriteBtn;
    private ImageView backIV,cartIV;
    private Food food;
    private CartFood cartFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grilled_chicken1);
        initializeViews();
        onclickListners();
        if (getIntent().getExtras() != null) {
            food = (Food) getIntent().getExtras().get(Constants.Bundle_Key.FOOD_DETAIL);
            configureFoodDescription();
        }

    }

    private void onclickListners() {
        backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        addtocartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FoodDescription.this, "Food is Added to the Cart", Toast.LENGTH_SHORT).show();
               final List<CartFood> cartFoodList = new ArrayList<>();
                CartFood cartFood = new CartFood();
                cartFood.setFoodname(food.getFoodname());
                cartFood.setFoodpic(food.getFoodpic());
                cartFood.setFoodcost(food.getFoodcost());
                cartFoodList.add(cartFood);
                cartFood.save();
//                SugarRecord.saveInTx(cartFoodList);

                List<CartFood> list = new DBCartFoodHandler().getAllFood();
                Log.e("Food desc", "cart size: " + list.size());
            }
        });
        addtofavoriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FoodDescription.this, "Food is Added to Favorite", Toast.LENGTH_SHORT).show();
                final List<FavoriteFood> favoriteFoodList = new ArrayList<>();
                FavoriteFood favoriteFood = new FavoriteFood();
                favoriteFood.setFoodname(food.getFoodname());
                favoriteFood.setFoodcost(food.getFoodcost());
                favoriteFood.setFoodpic(food.getFoodpic());
                favoriteFoodList.add(favoriteFood);
                favoriteFood.save();

            }
        });
        cartIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartFood!=null){
                    Intent intent = new Intent(FoodDescription.this,Dashboard.class);
                    intent.putExtra("EXTRA", "openFragment");
                    startActivity(intent);

                }
            }
        });

    }

    private void initializeViews() {
        foodnameTV = findViewById(R.id.foodNameTV);
        foodpriceTv = findViewById(R.id.foodPriceTV);
        foodpictureIV = findViewById(R.id.foodPicIV);
        fooddescriptionTV = findViewById(R.id.itemdescriptionTV);
        titlenameTV = findViewById(R.id.titlenameTV);
        backIV = findViewById(R.id.backIV);
        addtocartBtn = findViewById(R.id.addcartBtn);
        addtofavoriteBtn = findViewById(R.id.addfavoriteBtn);
        cartIV=findViewById(R.id.cartIV);
    }


    private void configureFoodDescription() {
        foodnameTV.setText(food.getFoodname());
        foodpriceTv.setText("Rs." + food.getFoodcost());
        Glide.with(FoodDescription.this).load(food.getFoodpic()).into(foodpictureIV);
        fooddescriptionTV.setText(food.getDescription());
        titlenameTV.setText(food.getFoodname());
    }
}
