package com.example.ayush.merokhaja.support.apiHandlers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.ayush.merokhaja.database.FoodType;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodHandler;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodTypeHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.AppController;
import com.example.ayush.merokhaja.utils.Constants;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FoodTypeApiHandler {
    String TAG = "FoodType";

    public void getFoodtype(Context context, final ApiListner apiListner) {
        String URL = Constants.MeroKhaja_Url.BASE_URL + Constants.MeroKhaja_Url.FOODTYPE_URL;
        Log.e(TAG, "food url: " + URL);
        StringRequest stringRequest = new StringRequest(URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                new DBFoodTypeHandler().deleteFoodType();
                try {
                    List<FoodType> foodTypeList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("FoodType");
                    Log.e(TAG,"rraylength"+jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject foodListobj = jsonArray.getJSONObject(i);
                        FoodType foodType = new FoodType();
                        foodType.setName(foodListobj.getString("name"));
                        foodType.setPicture(foodListobj.getString("image"));
                        foodTypeList.add(foodType);
                    }
                    SugarRecord.saveInTx(foodTypeList);
                    Log.e(TAG, "food response : " + foodTypeList.size());
                    apiListner.success(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                    apiListner.failure(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                apiListner.failure("failed : " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}

