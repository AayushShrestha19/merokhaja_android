package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.activity.FoodDescription;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.interfaces.CustomRecyclerClickListener;
import com.example.ayush.merokhaja.utils.Constants;

import java.util.List;

public class MenuRecylerViewAdapter extends RecyclerView.Adapter<MenuRecylerViewAdapter.MenuFoodViewHolder> {
    private Context context;
    private List<Food> foodList;
    private CustomRecyclerClickListener customRecyclerClickListener;

    public MenuRecylerViewAdapter(Context context, List<Food> foodList) {
        this.context = context;
        this.foodList = foodList;
    }

    @NonNull
    @Override
    public MenuFoodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.menu_food_view_items, viewGroup, false);
        MenuFoodViewHolder menuFoodViewHolder = new MenuFoodViewHolder(view);
        return menuFoodViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuFoodViewHolder menuFoodViewHolder, final int position) {
        final Food food = foodList.get(position);
        menuFoodViewHolder.foodnameTV.setText(food.getFoodname());
        menuFoodViewHolder.foodPriceTV.setText("Rs." + food.getFoodcost());
        Glide.with(context).load(food.getFoodpic()).into(menuFoodViewHolder.foodPicIV);


        menuFoodViewHolder.parentlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                customRecyclerClickListener.onClick(menuFoodViewHolder.parentlayout, position);
                Intent foodDetail = new Intent(context, FoodDescription.class);
                foodDetail.putExtra(Constants.Bundle_Key.FOOD_DETAIL, food);
                context.startActivity(foodDetail);
            }
        });
    }

//    public void setRecyclerItemClickListener(CustomRecyclerClickListener customRecyclerClickListener) {
//        this.customRecyclerClickListener = customRecyclerClickListener;
//    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public void updateList(List<Food> list) {
        foodList = list;
        notifyDataSetChanged();
    }

    class MenuFoodViewHolder extends RecyclerView.ViewHolder {
        private TextView foodnameTV;
        private ImageView foodPicIV;
        private TextView foodPriceTV;
        private RelativeLayout parentlayout;

        public MenuFoodViewHolder(@NonNull View itemView) {
            super(itemView);
            foodnameTV = itemView.findViewById(R.id.foodNameTV);
            foodPicIV = itemView.findViewById(R.id.foodPicIV);
            foodPriceTV = itemView.findViewById(R.id.foodPriceTV);
            parentlayout = itemView.findViewById(R.id.parentlayout);
        }
    }
}
