package com.example.ayush.merokhaja.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.activity.FoodDescription;
import com.example.ayush.merokhaja.activity.FoodList;
import com.example.ayush.merokhaja.adapter.MenuListViewAdapter;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.database.FoodType;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodHandler;
import com.example.ayush.merokhaja.database.databaseHandler.DBFoodTypeHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.apiHandlers.FoodApiHandler;
import com.example.ayush.merokhaja.support.apiHandlers.FoodTypeApiHandler;
import com.example.ayush.merokhaja.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodListFragment extends Fragment {

    private View view;
    private ListView menulistviewLV;
    private RecyclerView moremenuitemRV;
    SweetAlertDialog sweetAlertDialog;
    List<FoodType> menulist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_food_list, container, false);
        initializeView();
      /*  prepareMenuList();*/
        configureMenuItem();
        configureSweetLoadingDialog();
        getFoodTypeFromServer();
        geFoodFromServer();



        return view;
    }

    private void getFoodTypeFromServer() {
        showDialog();
        new FoodTypeApiHandler().getFoodtype(getContext(), new ApiListner() {
            @Override
            public void success(Object responce) {
                hideDialog();
                configureMenuItem();

            }

            @Override
            public void failure(Object responce) {

            }
        });
    }

    private void geFoodFromServer() {
        showDialog();
        new FoodApiHandler().getFoodList(getContext(), new ApiListner() {
            @Override
            public void success(Object responce) {
                hideDialog();
                setOnClickListner();

            }

            @Override
            public void failure(Object responce) {

            }
        });
    }

    private List<Food> filterFood(String filterString) {
        List<Food> foodList = new DBFoodHandler().getAllFood();
        List<Food> filteredFoodList = new ArrayList<>();
        for (Food food : foodList) {
            if (food.getFoodname().toUpperCase().contains(filterString.toUpperCase())) {
                filteredFoodList.add(food);
            }
        }
        return filteredFoodList;
    }
    private void configureMenuItem() {
        menulist = new DBFoodTypeHandler().getAllFood();
        Toast.makeText(getContext(), "size:"+menulist.size(), Toast.LENGTH_SHORT).show();
        MenuListViewAdapter adapter = new MenuListViewAdapter(getContext(), menulist);
        menulistviewLV.setAdapter(adapter);
    }
    private void setOnClickListner() {
        menulistviewLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getContext(), FoodList.class);
                intent.putExtra(Constants.Bundle_Key.FOOD_DETAIL, (Serializable) filterFood(menulist.get(position).getName()));
                Toast.makeText(getContext(), "menuList:"+menulist.get(position).getName(), Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }


    private void configureSweetLoadingDialog() {
        sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.NORMAL_TYPE).setTitleText("Loading...");
    }

    private void showDialog() {
        if (!sweetAlertDialog.isShowing()) {
            sweetAlertDialog.show();
        }
    }

    private void hideDialog() {
        if (sweetAlertDialog.isShowing()) {
            sweetAlertDialog.dismiss();
        }
    }




 /*   private void prepareMenuList() {

        menulist = new ArrayList<>();
        Food chowmein = new Food();
        chowmein.setFoodname("Chowmein");
        menulist.add(chowmein);

        Food momo = new Food();
        momo.setFoodname("MoMo");
        menulist.add(momo);

        Food thupka = new Food();
        thupka.setFoodname("Thukpa");
        menulist.add(thupka);

        Food burger = new Food();
        burger.setFoodname("Burger");
        menulist.add(burger);

        Food noodles = new Food();
        noodles.setFoodname("Noodles");
        menulist.add(noodles);

        Food sandwich = new Food();
        sandwich.setFoodname("Sandwich");
        menulist.add(sandwich);


    }*/

    private void initializeView() {
        moremenuitemRV = view.findViewById(R.id.moremenuitemRV);
        menulistviewLV = view.findViewById(R.id.menulistviewLV);


    }


}
