package com.example.ayush.merokhaja.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.adapter.NavigationAdapter;
import com.example.ayush.merokhaja.database.CartFood;
import com.example.ayush.merokhaja.database.User;
import com.example.ayush.merokhaja.database.databaseHandler.DBCartFoodHandler;
import com.example.ayush.merokhaja.database.databaseHandler.DBUserHandler;
import com.example.ayush.merokhaja.fragments.CartFragment;
import com.example.ayush.merokhaja.fragments.FavoriteFragment;
import com.example.ayush.merokhaja.fragments.FoodListFragment;
import com.example.ayush.merokhaja.fragments.MenuFragment;
import com.example.ayush.merokhaja.fragments.ProfileFragment;
import com.example.ayush.merokhaja.model.NavigationItem;
import com.example.ayush.merokhaja.support.AppController;
import com.example.ayush.merokhaja.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Dashboard extends AppCompatActivity {
    private android.support.v7.widget.Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private RelativeLayout navigationDrawerView;
    private ListView drawerItemList;
    private ImageView searchIV;
    private ImageView cartIV;
    private TextView userNameTV, emailTV;
    private TextView logoutTV;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private RelativeLayout headerLyout;
    private de.hdodenhof.circleimageview.CircleImageView userProfileIV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        initilizeViews();


        if (getIntent().getExtras() != null) {
            String type = getIntent().getExtras().getString("type");

            if (type == "") {
                //direct activity
            } else if (type == "") {
                //direct to another activity
            }
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(Dashboard.this);
        editor = preferences.edit();
        editor.putBoolean(Constants.SharedPreferenceConstants.HAS_USER, true);
        editor.apply();


        configureDrawerLayoutWidth();
        configureDrawerLayout();
        configureNavigationList();
        setUserData();
        setOnclickListners();
        replaceFragment(new MenuFragment());
//        getFragment();
    }

    private void getFragment() {
        switch (getIntent().getStringExtra("EXTRA")) {
            case "openFragment":
                replaceFragment(new CartFragment());
                break;
        }
    }

    private void setOnclickListners() {

        headerLyout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Account.class));
            }
        });


        logoutTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(Dashboard.this);
                progressDialog.setTitle("Logging out....");
                progressDialog.setCancelable(false);
                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        AppController.getInstance().deleteAllDBRecords();
                        editor.putBoolean(Constants.SharedPreferenceConstants.HAS_USER, false);
                        editor.apply();
                        startActivity(new Intent(Dashboard.this, Login.class));
                        finish();
                    }
                }, 2000);

            }
        });
        searchIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, SearchActivity.class));
            }
        });

        cartIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog alertDialog = new AlertDialog.Builder(Dashboard.this).create();
//                alertDialog.setTitle("NO item in cart");
//                alertDialog.setMessage("you need add food in cart first.");
//                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//
//                alertDialog.show();

                List<CartFood> foodList = new DBCartFoodHandler().getAllFood();
                if (foodList != null) {
                    replaceFragment(new CartFragment());
                } else {
                    showAlert();
                }

            }

        });
    }

    private void showAlert() {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(Dashboard.this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("No item in cart.\n Want to add some?")
                .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .setConfirmButton("Yes", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        Button yesBtn = sweetAlertDialog.findViewById(R.id.confirm_button);
        yesBtn.setBackground(getResources().getDrawable(R.drawable.sweet_alert_ok_btn));
    }

    private void configureDrawerLayoutWidth() {
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.7);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationDrawerView.getLayoutParams();
        params.width = width;
        navigationDrawerView.setLayoutParams(params);

    }

    private void configureDrawerLayout() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }

    private void configureNavigationList() {
        List<NavigationItem> navigationItemList = new ArrayList<>();

        NavigationItem navMenu = new NavigationItem();
        navMenu.setNavItemTitleTV("Menu");
        navMenu.setNavItemImageIV(R.drawable.ic_menu);
        navigationItemList.add(navMenu);

        NavigationItem navoffers = new NavigationItem();
        navoffers.setNavItemTitleTV("Offers");
        navoffers.setNavItemImageIV(R.drawable.ic_offer);
        navigationItemList.add(navoffers);

        NavigationItem navmycart = new NavigationItem();
        navmycart.setNavItemTitleTV("My Cart");
        navmycart.setNavItemImageIV(R.drawable.ic_mycart);
        navigationItemList.add(navmycart);

        NavigationItem navlastorder = new NavigationItem();
        navlastorder.setNavItemTitleTV("Last Order");
        navlastorder.setNavItemImageIV(R.drawable.ic_lastorder);
        navigationItemList.add(navlastorder);

        NavigationItem navfavorite = new NavigationItem();
        navfavorite.setNavItemTitleTV("Favorite");
        navfavorite.setNavItemImageIV(R.drawable.ic_favorite);
        navigationItemList.add(navfavorite);

        NavigationItem navsettings = new NavigationItem();
        navsettings.setNavItemTitleTV("Profile");
        navsettings.setNavItemImageIV(R.drawable.ic_setting);
        navigationItemList.add(navsettings);

        NavigationItem navsupport = new NavigationItem();
        navsupport.setNavItemTitleTV("Support");
        navsupport.setNavItemImageIV(R.drawable.ic_support);
        navigationItemList.add(navsupport);


        NavigationAdapter adapter = new NavigationAdapter(this, navigationItemList);
        drawerItemList.setAdapter(adapter);
        drawerItemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        replaceFragment(new FoodListFragment());
                        closeDrawer();
                        break;
                    case 1:
                        closeDrawer();
                        break;
                    case 2:
                        replaceFragment(new CartFragment());
                        closeDrawer();
                    case 3:
                        closeDrawer();
                        break;
                    case 4:
                        replaceFragment(new FavoriteFragment());
                        closeDrawer();
                        break;
                    case 5:

                        replaceFragment(new ProfileFragment());
                        closeDrawer();
                        break;


                }
            }
        });

    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(Gravity.START);

    }

    private void initilizeViews() {
        userProfileIV = findViewById(R.id.userProfileIV);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationDrawerView = findViewById(R.id.navigationDrawerView);
        drawerItemList = findViewById(R.id.drawerItemList);
        userNameTV = findViewById(R.id.userNameTV);
        emailTV = findViewById(R.id.emailTV);
        searchIV = findViewById(R.id.searchIV);
        logoutTV = findViewById(R.id.logoutTV);
        headerLyout = findViewById(R.id.headerLyout);
        cartIV = findViewById(R.id.cartIV);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentTransaction.replace(R.id.fragmentContainer, fragment);
        fragmentTransaction.commit();
    }

    private void setUserData() {
        User user = new DBUserHandler().getAllUsers().get(0);
        userNameTV.setText(user.getName());
        emailTV.setText(user.getEmail());
        Glide.with(Dashboard.this).load(user.getImage()).into(userProfileIV);

    }
}
