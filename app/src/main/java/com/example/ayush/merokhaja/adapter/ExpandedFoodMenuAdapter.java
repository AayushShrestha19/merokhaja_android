package com.example.ayush.merokhaja.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.activity.FoodDescription;
import com.example.ayush.merokhaja.activity.FoodList;
import com.example.ayush.merokhaja.database.Food;
import com.example.ayush.merokhaja.utils.Constants;

import java.util.List;

public class ExpandedFoodMenuAdapter extends RecyclerView.Adapter<ExpandedFoodMenuAdapter.ExpandedFoodMenuViewHolder> {

    private Context context;
    private List<Food> foodList;

    public ExpandedFoodMenuAdapter(Context context , List<Food> foodList) {
        this.context=context;
        this.foodList=  foodList;
    }

    @NonNull
    @Override
    public ExpandedFoodMenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.expanded_food_menu_item,viewGroup,false);
        ExpandedFoodMenuViewHolder expandedFoodMenuViewHolder = new ExpandedFoodMenuViewHolder(view);

        return expandedFoodMenuViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExpandedFoodMenuViewHolder expandedFoodMenuViewHolder, int position) {
        final Food food = foodList.get(position);
        expandedFoodMenuViewHolder.foodnameTV.setText(food.getFoodname());
        expandedFoodMenuViewHolder.foodpriceTV.setText("Rs." + food.getFoodcost());
        Glide.with(context).load(food.getFoodpic()).into(expandedFoodMenuViewHolder.foodpicIV);

        expandedFoodMenuViewHolder.parentlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent foodDetail = new Intent(context, FoodDescription.class);
                foodDetail.putExtra(Constants.Bundle_Key.FOOD_DETAIL, food);
                context.startActivity(foodDetail);
            }
        });


    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public class ExpandedFoodMenuViewHolder extends RecyclerView.ViewHolder {

        private ImageView foodpicIV;
        private TextView foodnameTV;
        private TextView foodpriceTV;
        private RelativeLayout parentlayout;
        public ExpandedFoodMenuViewHolder(@NonNull View itemView) {
            super(itemView);
            foodnameTV = itemView.findViewById(R.id.foodnameTV);
            foodpriceTV=itemView.findViewById(R.id.foodpriceTV);
            foodpicIV=itemView.findViewById(R.id.foodpicIV);
            parentlayout=itemView.findViewById(R.id.parentlayout);
        }
    }
}
