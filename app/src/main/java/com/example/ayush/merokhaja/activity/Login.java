package com.example.ayush.merokhaja.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.database.User;
import com.example.ayush.merokhaja.database.databaseHandler.DBUserHandler;
import com.example.ayush.merokhaja.interfaces.ApiListner;
import com.example.ayush.merokhaja.support.apiHandlers.LoginApiHandler;
import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Login extends AppCompatActivity {

    private String TAG = "Login";
    private EditText emailET, passwordET;
    private Button signInBtn;
    private TextView signUpTV;
    private SweetAlertDialog sweetProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeViews();
        emailET.setText("john_doe@gmail.com");
        passwordET.setText("johnDoe123");
        initializeListeners();
        prepareSweetProgressDialog();
        onclicListners();
    }

    private void onclicListners() {
        signUpTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent (Login.this,SignUp.class));
            }
        });
    }


    private void prepareSweetProgressDialog() {
        sweetProgressDialog = new SweetAlertDialog(Login.this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Logging In...");
    }

    private void showProgressDialog() {
        if (sweetProgressDialog != null && !sweetProgressDialog.isShowing()) {
            sweetProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (sweetProgressDialog != null && sweetProgressDialog.isShowing()) {
            sweetProgressDialog.dismiss();
        }
    }

    private void initializeViews() {
        emailET = findViewById(R.id.emailET);
        passwordET = findViewById(R.id.passwordET);
        signInBtn = findViewById(R.id.signInBtn);
        signUpTV = findViewById(R.id.siTV);
    }

    private void initializeListeners() {
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateLoginData(emailET.getText().toString(), passwordET.getText().toString())) {
                    showProgressDialog();
                    new LoginApiHandler().login(Login.this, new ApiListner() {
                        @Override
                        public void success(Object responce) {
                            hideProgressDialog();
                            boolean status = new DBUserHandler().checkUser(emailET.getText().toString(), passwordET.getText().toString());
                            List<User> userList = new DBUserHandler().getAllUsers();
                            Log.e(TAG, "user list size: " + userList.size());
                            if (status) {
                                // login successs
                                Toast.makeText(Login.this, "success login", Toast.LENGTH_SHORT).show();

                                startActivity(new Intent(Login.this, Dashboard.class));
                                finish();
                            } else {
                                //user doesnt exists
                                Toast.makeText(Login.this, "success failed", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void failure(Object responce) {
                            hideProgressDialog();
                            Toast.makeText(Login.this, responce.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else
                    Toast.makeText(Login.this, "please enter valid data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateLoginData(String email, String password) {
        if (!email.equalsIgnoreCase("") || !password.equalsIgnoreCase("")) {
            return true;
        } else {
            return false;
        }
    }
}
