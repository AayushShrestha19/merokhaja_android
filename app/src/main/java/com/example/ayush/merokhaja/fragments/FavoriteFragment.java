package com.example.ayush.merokhaja.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ayush.merokhaja.R;
import com.example.ayush.merokhaja.adapter.CartRecyclerViewAdapter;
import com.example.ayush.merokhaja.adapter.FavoriteRecyclerViewAdapter;
import com.example.ayush.merokhaja.database.FavoriteFood;
import com.example.ayush.merokhaja.database.databaseHandler.DBFavoriteFood;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {
    private View view;
    private RecyclerView favoriterecyclerviewRV;


    public FavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_favorite, container, false);
        initialiseviews();
        configureRecyclerView();
        return view;
    }

    private void configureRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        favoriterecyclerviewRV.setLayoutManager(linearLayoutManager);
        List<FavoriteFood> favoriteFood = new DBFavoriteFood().getAllFood();
        FavoriteRecyclerViewAdapter adapter = new FavoriteRecyclerViewAdapter(getContext(), favoriteFood);
        favoriterecyclerviewRV.setAdapter(adapter);
    }

    private void initialiseviews() {
        favoriterecyclerviewRV=view.findViewById(R.id.favoriterecyclerviewRV);
    }

}
