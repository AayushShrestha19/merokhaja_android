package com.example.ayush.merokhaja.database;

import com.orm.SugarRecord;

import java.io.Serializable;

public class CartFood extends SugarRecord implements Serializable {

    String foodname;
    String foodpic;
    int foodcost;

    public String getFoodname() {
        return foodname;
    }

    public void setFoodname(String foodname) {
        this.foodname = foodname;
    }

    public String getFoodpic() {
        return foodpic;
    }

    public void setFoodpic(String foodpic) {
        this.foodpic = foodpic;
    }

    public int getFoodcost() {
        return foodcost;
    }

    public void setFoodcost(int foodcost) {
        this.foodcost = foodcost;
    }
}